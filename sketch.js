var vertices = [];

Array.prototype.clone = function() {
	return this.slice(0);
};

function init(x) {
	for (var i = 0; i < x; i++) {
		vertices.push(createVector(random(8, width-7), random(8, height-7)));
	}
}

function setup() {
	createCanvas(640, 360);	
	init(50);
}

function mousePressed() {
	var v = createVector(mouseX, mouseY);
	vertices.push(v);
}

function draw() 
{
	background(51);
	drawText("PRIM", (width/2)-25, 15);


	var reached = [];
	var unreached = vertices.clone();
	var tmp = vertices.clone();

	reached.push(unreached[0]);
	unreached.splice(0, 1);

	while(unreached.length > 0)
	{
		var record = dist(0, 0, width, height);
		var rIndex;
		var uIndex;
		for (var i = 0; i < reached.length; i++) 
		{
			for (var j = 0; j < unreached.length; j++) 
			{	
				var v1 = reached[i];
				var v2 = unreached[j];			
				var d = dist(v1.x, v1.y, v2.x, v2.y);
				
				if (d < record) 
				{
					record = d;
					rIndex = i;
					uIndex = j;
				}
			}
		}	
		stroke(255);
		strokeWeight(2);
		line(reached[rIndex].x, reached[rIndex].y, unreached[uIndex].x, unreached[uIndex].y);
		reached.push(unreached[uIndex]);
		unreached.splice(uIndex, 1);
	}

	for (var i = 0; i < vertices.length; i++) 
	{
		fill(255,0,0);
		stroke(255,0,0);
		ellipse(vertices[i].x, vertices[i].y, 8, 8);
		drawText(i+1,vertices[i].x, vertices[i].y);
	}

}

function drawText(t,x,y) {
	fill(255);
	stroke(0);
	text(t, x, y);
}